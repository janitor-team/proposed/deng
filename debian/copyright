Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Source: https://dengine.net/source
Upstream-Name: Doomsday Engine
Upstream-Contact: https://talk.dengine.net/
Files-Excluded:
 doomsday/external/zlib
 doomsday/external/assimp
 doomsday/external/fluidsynth
# doomsday/client/net.dengine.client.pack/defaultstyle.pack/fonts
# doomsday/tests/test_appfw/net.dengine.test.appfw.pack/defaultstyle.pack/fonts

Files: *
Copyright: 1986, Gary S. Brown
  1993-2006, id Software, Inc
  1995-2000, Brian Paul
  1998-2000, Colin Reed <cph@moria.org.uk>
  1998-2000, Lee Killough <killough@rsn.hp.com>
  1999, Activision
  1999, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman (PrBoom 2.2.6)
  1999-2000, Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze (PrBoom 2.2.6)
  1999-2013, Jaakko Keränen <jaakko.keranen@iki.fi>
  2000-2007, Andrew Apted <ajapted@gmail.com>
  2003-2005, Samuel Villarreal <svkaiser@gmail.com>
  2003-2012, Deng Team
  2004, Lukasz Stelmach
  2005, Zachary Keene <zjkeene@bellsouth.net>
  2005-2013, Daniel Swanson <danij@dengine.net>
  2006, Martin Eyre <martineyre@btinternet.com>
  2006-2008, Jamie Jones <jamie_jones_au@yahoo.com.au>
License: GPL-2+

Files: doomsday/apps/client/res/linux/net.dengine.Doomsday.metainfo.xml
Copyright: 2019 Jaakko Keränen <jaakko.keranen@iki.fi>
License: CC0-1.0

Files:
 doomsday/sdk/libshell/include/de/shell/textwidget.h
 doomsday/sdk/libappfw/include/de/framework/persistentstate.h
 doomsday/sdk/libgui/include/de/gui/displaymode.h
Copyright: Copyright (c) 2012-2017 Jaakko Keränen <jaakko.keranen@iki.fi>
License: LGPL-3+

Files:
 doomsday/sdk/libgui/src/input/imKStoUCS_x11.c
Copyright: 1994-2003, The XFree86 Project, Inc.
License: Expat

Files: doomsday/external/lzss/*
Copyright: 1989 Haruhiko Okumura
License: Public-Domain

Files:
 doomsday/apps/plugins/fmod/*
Copyright: 2011-2013, Jaakko Keränen <jaakko.keranen@iki.fi>
License: GPL-2+ with FMOD Ex exception
 The Doomsday Audio Plugin for FMOD Ex (audio_fmod) is licensed under the terms
 of the GNU General Public License version 2, with a special exception granted
 by the copyright holders of audio_fmod to link against the non-free "FMOD Ex
 Programmer's API" and distribute the resulting product as permitted by the
 licenses.
 .
 .
              SPECIAL EXCEPTION TO THE GPLv2 LICENSE
 .
 Linking the Doomsday Audio Plugin for FMOD Ex (audio_fmod) statically or
 dynamically with other modules is making a combined work based on audio_fmod.
 Thus, the terms and conditions of the GNU General Public License cover the
 whole combination.
 .
 In addition, AS A SPECIAL EXCEPTION, the copyright holders of audio_fmod give
 you permission to combine audio_fmod with free software programs or libraries
 that are released under the GNU LGPL and with code included in the standard
 release of "FMOD Ex Programmer's API" under the "FMOD Ex Programmer's API"
 license (or modified versions of such code, with unchanged license). You may
 copy and distribute such a system following the terms of the GNU GPL for
 audio_fmod and the licenses of the other code concerned, provided that you
 include the source code of that other code when and as the GNU GPL requires
 distribution of source code.
 .
 Note that people who make modified versions of audio_fmod are not obligated to
 grant this special exception for their modified versions; it is their choice
 whether to do so. The GNU General Public License gives permission to release a
 modified version without this exception; this exception also makes it possible
 to release a modified version which carries forward this exception.

Files: debian/*
Copyright:
  2010-2012 Kees Meijs <post@keesmeijs.nl>
  2013-2014 Fabian Greffrath <fabian+debian@greffrath.com>
  2015 Michael Gilbert <mgilbert@debian.org>
License: GPL-2+

License: Public-Domain
 Public domain software.

License: CC0-1.0
 On Debian systems, the full text of the CC0 1.0 Universal
 License can be found in the file
 "/usr/share/common-licenses/CC0-1.0".

License: GPL-2+
 See /usr/share/common-licenses/GPL-2.

License: LGPL-3+
 See /usr/share/common-licenses/LGPL-3.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is fur-
 nished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FIT-
 NESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 XFREE86 PROJECT BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CON-
 NECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the XFree86 Project shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the XFree86
 Project.
